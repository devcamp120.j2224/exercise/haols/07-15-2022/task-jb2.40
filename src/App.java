import com.devcamp.S10.jbr240.Person;
import com.devcamp.S10.jbr240.Staff;
import com.devcamp.S10.jbr240.Student;

public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person("Manh", "Q2, HCM");
        Person person2 = new Person("Long", "Tan Binh, HCM");
        System.out.println(person1.toString());
        System.out.println(person2.toString());

        Student student1 = new Student("Kha", "Ha Noi", "English", 2011, 3000);
        Student student2 = new Student("Hung", "Binh Duong", "Java", 2015, 2500);
        System.out.println(student1.toString());
        System.out.println(student2.toString());

        Staff staff1 = new Staff("Ha", "HCM", "Vin", 5000);
        Staff staff2 = new Staff("Hao", "Long An", "RNIT", 10000);
        System.out.println(staff1.toString());
        System.out.println(staff2.toString());
    }
}
